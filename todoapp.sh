#!/bin/bash

echo Creating 100 todos

for ((i = 0; i < 100; i++)); do
  curl --request POST -sL \
    --url 'http://todoapp-env.eba-scgmihwz.eu-central-1.elasticbeanstalk.com/api/v1/todos' \
    -H "Content-Type: application/json" \
    --data "{\"taskHeader\":\"Header $i\", \"description\":\"description $i\", \"deadline\":\"2021-10-18\"}"
done

echo Getting all todos

curl --request GET -sL \
  --url 'http://todoapp-env.eba-scgmihwz.eu-central-1.elasticbeanstalk.com/api/v1/todos' | jq

read -rp 'Please enter todo id ' todoid

echo Getting todo with id "$todoid"

curl --request GET -sL \
  --url "http://todoapp-env.eba-scgmihwz.eu-central-1.elasticbeanstalk.com/api/v1/todos/{$todoid}" | jq

read -rp  'Please enter todo id to be updated ' updatedid

echo Updating todo with id "$updatedid"

curl --request PUT -sL \
    --url "http://todoapp-env.eba-scgmihwz.eu-central-1.elasticbeanstalk.com/api/v1/todos/{$updatedid}" \
    -H "Content-Type: application/json" \
    --data "{\"taskHeader\":\"Updated\", \"description\":\"updated\", \"deadline\":\"2021-10-18\", \"state\":\"WORK_IN_PROGRESS\"}"

echo Getting all ToDos with WORK_IN_PROGRESS state

curl --request GET -sL \
    --url "http://todoapp-env.eba-scgmihwz.eu-central-1.elasticbeanstalk.com/api/v1/todos?state=WORK_IN_PROGRESS" | jq

read -rp  'Please enter todo id to be deleted ' deleteid

echo Delating todo with id "${deleteid}"


curl --request DELETE -sL \
    --url "http://todoapp-env.eba-scgmihwz.eu-central-1.elasticbeanstalk.com/api/v1/todos/${deleteid}"
