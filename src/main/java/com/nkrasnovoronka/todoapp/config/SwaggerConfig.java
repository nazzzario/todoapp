package com.nkrasnovoronka.todoapp.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenApi() {
        return new OpenAPI()
                .info(new Info()
                        .title("Todo application")
                        .version("1.0")
                        .description("Simple Spring Boot application")
                        .contact(new Contact()
                                .email("nazar.krasnovoronka@outlook.com")
                                .name("Nazar Krasnovoronka"))
                        .license(new License()
                                .name("Open license"))
                );
    }

}
