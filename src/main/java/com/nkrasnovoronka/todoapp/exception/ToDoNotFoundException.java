package com.nkrasnovoronka.todoapp.exception;

public class ToDoNotFoundException extends RuntimeException {

    public ToDoNotFoundException() {
        super();
    }

    public ToDoNotFoundException(String message) {
        super(message);
    }
}
