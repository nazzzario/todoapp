package com.nkrasnovoronka.todoapp.model;


import com.nkrasnovoronka.todoapp.exception.InvalidStateException;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public enum State {
    PLANNED, WORK_IN_PROGRESS, DONE, CANCELLED;

    private static final Map<State, Set<State>> STATES_CHANGES_MAP = new EnumMap<>(State.class);

    static {
        STATES_CHANGES_MAP.put(PLANNED, Set.of(WORK_IN_PROGRESS));
        STATES_CHANGES_MAP.put(WORK_IN_PROGRESS, Set.of(DONE, CANCELLED));
    }

    public static void checkIfStateValid(State currentState, State changeTo) {
        if (!currentState.equals(changeTo)) {
            boolean isCorrectState = Optional.ofNullable(STATES_CHANGES_MAP.get(currentState))
                    .map(states -> states.contains(changeTo))
                    .orElseThrow(() -> new IllegalArgumentException("Cannot find state " + changeTo));
            if (!isCorrectState) {
                throw new InvalidStateException(String.format("Cannot change state from %s to %s", currentState, changeTo));
            }
        }
    }


}
