package com.nkrasnovoronka.todoapp.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "todos")
public class TodoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "task_header", nullable = false)
    private String taskHeader;

    private String description;

    private LocalDate deadline;

    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    @PrePersist
    public void prePersist() {
        state = State.PLANNED;
        createdOn = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        updatedOn = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskHeader() {
        return taskHeader;
    }

    public void setTaskHeader(String taskHeader) {
        this.taskHeader = taskHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TodoEntity)) return false;
        TodoEntity todoEntity = (TodoEntity) o;
        return Objects.equals(id, todoEntity.id) && Objects.equals(taskHeader, todoEntity.taskHeader) && Objects.equals(description, todoEntity.description) && Objects.equals(deadline, todoEntity.deadline) && state == todoEntity.state && Objects.equals(createdOn, todoEntity.createdOn) && Objects.equals(updatedOn, todoEntity.updatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taskHeader, description, deadline, state, createdOn, updatedOn);
    }

    @Override
    public String toString() {
        return "TodoEntity{" +
                "id=" + id +
                ", taskHeader='" + taskHeader + '\'' +
                ", description='" + description + '\'' +
                ", deadline=" + deadline +
                ", state=" + state +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
