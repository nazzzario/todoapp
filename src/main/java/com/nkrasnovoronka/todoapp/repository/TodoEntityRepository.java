package com.nkrasnovoronka.todoapp.repository;

import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.model.TodoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoEntityRepository extends JpaRepository<TodoEntity, Long> {

    List<TodoEntity> findAllByState(State state);

}
