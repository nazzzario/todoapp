package com.nkrasnovoronka.todoapp.controller;

import com.nkrasnovoronka.todoapp.dto.CreateTodoDTO;
import com.nkrasnovoronka.todoapp.dto.TodoDTO;
import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.service.TodoEntityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/todos")
@Tag(name = "ToDoList controller", description = "Do actions with ToDoList")
public class TodoController {

    private final TodoEntityService todoEntityService;
    private final Logger logger = LoggerFactory.getLogger(TodoController.class);

    @Autowired
    public TodoController(TodoEntityService todoEntityService) {
        this.todoEntityService = todoEntityService;
    }

    @Operation(summary = "Get list of all ToDo tasks")
    @ApiResponse(responseCode = "200", description = "Return all todo tasks",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = TodoDTO.class))}
    )
    @GetMapping
    public List<TodoDTO> getAllTodos() {
        logger.info("Getting all todos");
        return todoEntityService.getAllTodos();
    }

    @Operation(summary = "Get list of all ToDo tasks by state")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Return all todo tasks with state",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = TodoDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid state supplied", content = @Content)
    })
    @GetMapping(params = "state")
    public List<TodoDTO> getAllTodosByState(@Parameter(description = "Name of ToDo state to be founded") @RequestParam(required = false) State state) {
        logger.info("Getting all todos with state {}", state);
        return todoEntityService.getAllTodosByState(state);
    }

    @Operation(summary = "Get Todo by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found ToDo",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = TodoDTO.class))),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "ToDo not found", content = @Content)

    })
    @GetMapping("/{id}")
    public TodoDTO getTodoById(@Parameter(description = "id of the ToDo to be searched") @PathVariable Long id) {
        logger.info("Getting ToDo with id {}", id);
        return todoEntityService.getTodoById(id);
    }

    @Operation(summary = "Delete Todo by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Delete ToDo",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "ToDo not found", content = @Content)

    })
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodoById(@Parameter(description = "id of the ToDo to be deleted")
                               @PathVariable Long id) {
        logger.info("Deleting ToDo with id {}", id);
        todoEntityService.deleteTodosById(id);
    }

    @Operation(summary = "Create Todo")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Create ToDo",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "400", description = "Invalid ToDo values", content = @Content)
    })
    @PostMapping
    public void createTodo(@Parameter(description = "ToDo to be created") @Valid @RequestBody CreateTodoDTO todoListDTO) {
        logger.info("Creating new ToDo {}", todoListDTO);
        todoEntityService.createTodos(todoListDTO);
    }

    @Operation(summary = "Update Todo")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Update ToDo",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "ToDo not found", content = @Content)
    })
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoDTO updateTodo(@Parameter(description = "id of the ToDo to be updated") @PathVariable Long id,
                              @Parameter(description = "ToDo fields to be updated") @RequestBody @Valid TodoDTO todoDTO) {
        logger.info("Updating ToDo with id {} to {}", id, todoDTO);
        return todoEntityService.updateTodos(id, todoDTO);
    }
}
