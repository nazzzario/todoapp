package com.nkrasnovoronka.todoapp.service;

import com.nkrasnovoronka.todoapp.dto.CreateTodoDTO;
import com.nkrasnovoronka.todoapp.dto.TodoDTO;
import com.nkrasnovoronka.todoapp.model.State;

import java.util.List;

public interface TodoEntityService {
    List<TodoDTO> getAllTodos();

    List<TodoDTO> getAllTodosByState(State state);

    void createTodos(CreateTodoDTO createdTodos);

    TodoDTO updateTodos(Long id, TodoDTO updated);

    TodoDTO getTodoById(Long id);

    void deleteTodosById(Long id);

}
