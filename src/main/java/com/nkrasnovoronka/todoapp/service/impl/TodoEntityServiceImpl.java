package com.nkrasnovoronka.todoapp.service.impl;

import com.nkrasnovoronka.todoapp.dto.CreateTodoDTO;
import com.nkrasnovoronka.todoapp.dto.TodoDTO;
import com.nkrasnovoronka.todoapp.exception.ToDoNotFoundException;
import com.nkrasnovoronka.todoapp.mapper.TodoEntityMapper;
import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.model.TodoEntity;
import com.nkrasnovoronka.todoapp.repository.TodoEntityRepository;
import com.nkrasnovoronka.todoapp.service.TodoEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoEntityServiceImpl implements TodoEntityService {

    private final TodoEntityRepository todoEntityRepository;
    private final Logger logger = LoggerFactory.getLogger(TodoEntityServiceImpl.class);

    @Autowired
    public TodoEntityServiceImpl(TodoEntityRepository todoEntityRepository) {
        this.todoEntityRepository = todoEntityRepository;
    }


    @Override
    @Transactional(readOnly = true)
    public List<TodoDTO> getAllTodos() {
        return todoEntityRepository
                .findAll()
                .stream()
                .map(TodoEntityMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<TodoDTO> getAllTodosByState(State state) {
        return Optional.ofNullable(state)
                .map(todoEntityRepository::findAllByState)
                .orElseThrow(() -> new ToDoNotFoundException("Cannot find ToDos with state " + state))
                .stream()
                .map(TodoEntityMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void createTodos(CreateTodoDTO createdTodos) {
        TodoEntity created = Optional.ofNullable(createdTodos)
                .map(TodoEntityMapper::toEntity)
                .orElseThrow(() -> new IllegalArgumentException("Cannot create ToDo"));
        todoEntityRepository.save(created);
    }

    @Override
    @Transactional
    public TodoDTO updateTodos(Long id, TodoDTO updatedDto) {
        TodoEntity byId = todoEntityRepository.findById(id)
                .orElseThrow(() -> new ToDoNotFoundException("Cannot find ToDo with id " + id));
        TodoEntity updated = TodoEntityMapper.updateEntity(byId, updatedDto);
        todoEntityRepository.save(updated);
        return updatedDto;
    }

    @Override
    @Transactional
    public TodoDTO getTodoById(Long id) {
        return todoEntityRepository.findById(id)
                .map(TodoEntityMapper::toDto)
                .orElseThrow(() -> new ToDoNotFoundException("Cannot find ToDo with id " + id));
    }

    @Override
    @Transactional
    public void deleteTodosById(Long id) {
        todoEntityRepository.deleteById(id);
    }
}
