package com.nkrasnovoronka.todoapp.mapper;

import com.nkrasnovoronka.todoapp.dto.CreateTodoDTO;
import com.nkrasnovoronka.todoapp.dto.TodoDTO;
import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.model.TodoEntity;

import java.util.Objects;

public final class TodoEntityMapper {

    private TodoEntityMapper() {
    }

    public static TodoEntity toEntity(TodoDTO todoDTO) {
        TodoEntity todoEntity = new TodoEntity();
        todoEntity.setTaskHeader(todoDTO.getTaskHeader());
        todoEntity.setDescription(todoDTO.getDescription());
        todoEntity.setDeadline(todoDTO.getDeadline());
        todoEntity.setState(todoDTO.getState());
        return todoEntity;
    }

    public static TodoEntity toEntity(CreateTodoDTO todoListDTO) {
        TodoEntity todoEntity = new TodoEntity();
        todoEntity.setTaskHeader(todoListDTO.getTaskHeader());
        todoEntity.setDescription(todoListDTO.getDescription());
        todoEntity.setDeadline(todoListDTO.getDeadline());
        return todoEntity;
    }

    public static TodoDTO toDto(TodoEntity todoEntity) {
        TodoDTO todoDTO = new TodoDTO();
        todoDTO.setTaskHeader(todoEntity.getTaskHeader());
        todoDTO.setDescription(todoEntity.getDescription());
        todoDTO.setDeadline(todoEntity.getDeadline());
        todoDTO.setState(todoEntity.getState());
        return todoDTO;
    }

    public static TodoEntity updateEntity(TodoEntity currentEntity, TodoDTO updatedEntity) {
        currentEntity.setTaskHeader(Objects.requireNonNullElse(updatedEntity.getTaskHeader(), currentEntity.getTaskHeader()));
        currentEntity.setDescription(Objects.requireNonNullElse(updatedEntity.getDescription(), currentEntity.getDescription()));
        currentEntity.setDeadline(Objects.requireNonNullElse(updatedEntity.getDeadline(), currentEntity.getDeadline()));
        if (Objects.nonNull(updatedEntity.getState())) {
            State.checkIfStateValid(currentEntity.getState(), updatedEntity.getState());
            currentEntity.setState(updatedEntity.getState());
        }
        return currentEntity;
    }

}
