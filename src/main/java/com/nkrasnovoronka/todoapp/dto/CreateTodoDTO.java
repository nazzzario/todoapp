package com.nkrasnovoronka.todoapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;

@Schema(description = "ToDo to be created")
public class CreateTodoDTO {
    @Size(max = 100)
    @NotBlank(message = "Task header cannot be empty")
    @Schema(example = "Go to the shop")
    private String taskHeader;

    @Schema(example = "By milk")
    private String description;

    @FutureOrPresent(message = "Invalid deadline date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Schema(example = "2021-03-12")
    private LocalDate deadline;

    public String getTaskHeader() {
        return taskHeader;
    }

    public void setTaskHeader(String taskHeader) {
        this.taskHeader = taskHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreateTodoDTO)) return false;
        CreateTodoDTO that = (CreateTodoDTO) o;
        return Objects.equals(taskHeader, that.taskHeader) && Objects.equals(description, that.description) && Objects.equals(deadline, that.deadline);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskHeader, description, deadline);
    }

    @Override
    public String toString() {
        return "CreateTodoListDTO{" +
                "taskHeader='" + taskHeader + '\'' +
                ", description='" + description + '\'' +
                ", deadline=" + deadline +
                '}';
    }
}
