package com.nkrasnovoronka.todoapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nkrasnovoronka.todoapp.model.State;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;

@Schema(description = "ToDo entity")
public class TodoDTO {
    @Size(max = 100)
    @NotBlank(message = "Task header cannot be empty")
    @Schema(example = "Go to the shop")
    private String taskHeader;

    @Schema(example = "By milk")
    private String description;

    @FutureOrPresent(message = "Invalid deadline date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Schema(example = "2021-03-12")
    private LocalDate deadline;

    @NotNull
    @Schema(example = "WORK_IN_PROGRESS")
    private State state;

    public String getTaskHeader() {
        return taskHeader;
    }

    public void setTaskHeader(String taskHeader) {
        this.taskHeader = taskHeader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TodoDTO)) return false;
        TodoDTO that = (TodoDTO) o;
        return Objects.equals(taskHeader, that.taskHeader)
                && Objects.equals(description, that.description)
                && Objects.equals(deadline, that.deadline)
                && state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskHeader, description, deadline, state);
    }

    @Override
    public String toString() {
        return "TodoListDTO{" +
                "taskHeader='" + taskHeader + '\'' +
                ", description='" + description + '\'' +
                ", deadline=" + deadline +
                ", state=" + state +
                '}';
    }
}
