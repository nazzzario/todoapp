package com.nkrasnovoronka.todoapp.controller;

import com.nkrasnovoronka.todoapp.dto.CreateTodoDTO;
import com.nkrasnovoronka.todoapp.dto.TodoDTO;
import com.nkrasnovoronka.todoapp.exception.ToDoNotFoundException;
import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.service.impl.TodoEntityServiceImpl;
import com.nkrasnovoronka.todoapp.util.ObjectToJson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = TodoController.class)
class TodoControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private TodoEntityServiceImpl todoEntityService;


    @Test
    void getAllTodos() throws Exception {
        when(todoEntityService.getAllTodos()).thenReturn(List.of(new TodoDTO()));

        mockMvc.perform(get("https://localhost:8080/api/v1/todos")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getAllTodosByState() throws Exception {
        when(todoEntityService.getAllTodosByState(any(State.class))).thenReturn(List.of(new TodoDTO()));

        mockMvc.perform(get("https://localhost:8080/api/v1/todos")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("state", "PLANNED"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void sendInvalidState() throws Exception {
        when(todoEntityService.getAllTodosByState(any(State.class))).thenReturn(List.of(new TodoDTO()));

        mockMvc.perform(get("https://localhost:8080/api/v1/todos")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("state", "WRONG_STATE"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void getToDoById() throws Exception {
        TodoDTO expected = new TodoDTO();
        expected.setTaskHeader("Header");
        expected.setDescription("Description");
        expected.setState(State.PLANNED);
        expected.setDeadline(LocalDate.parse("2040-10-16"));

        when(todoEntityService.getTodoById(anyLong())).thenReturn(expected);

        mockMvc.perform(get("https://localhost:8080/api/v1/todos/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.taskHeader").value("Header"))
                .andExpect(jsonPath("$.description").value("Description"))
                .andExpect(jsonPath("$.state").value("PLANNED"))
                .andExpect(jsonPath("$.deadline").value("2040-10-16"));
    }

    @Test
    void voidInvalidIdSupplied() throws Exception {
        mockMvc.perform(get("https://localhost:8080/api/v1/todos/text")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void todoNotFoundTest() throws Exception {
        when(todoEntityService.getTodoById(anyLong())).thenThrow(ToDoNotFoundException.class);

        mockMvc.perform(get("https://localhost:8080/api/v1/todos/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteTodoTest() throws Exception {
        doNothing().when(todoEntityService).deleteTodosById(anyLong());

        mockMvc.perform(delete("https://localhost:8080/api/v1/todos/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    void createTodoTest() throws Exception {
        CreateTodoDTO expected = new CreateTodoDTO();
        expected.setTaskHeader("Header");
        expected.setDescription("Description");
        expected.setDeadline(LocalDate.parse("2040-10-16"));


        doNothing().when(todoEntityService).createTodos(any(CreateTodoDTO.class));

        mockMvc.perform(post("https://localhost:8080/api/v1/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(ObjectToJson.asJsonString(expected))
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void creatingInvalidTodo() throws Exception {
        CreateTodoDTO expected = new CreateTodoDTO();
        expected.setTaskHeader("Header");
        expected.setDescription("Description");
        expected.setDeadline(LocalDate.parse("1808-10-16"));


        doNothing().when(todoEntityService).createTodos(any(CreateTodoDTO.class));

        mockMvc.perform(post("https://localhost:8080/api/v1/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(ObjectToJson.asJsonString(expected))
                )
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void updatingTodo() throws Exception {
        TodoDTO todoDTO = new TodoDTO();
        todoDTO.setTaskHeader("Header");
        todoDTO.setDescription("Description");
        todoDTO.setState(State.PLANNED);
        todoDTO.setDeadline(LocalDate.parse("2040-10-16"));


        when(todoEntityService.updateTodos(anyLong(), any(TodoDTO.class))).thenReturn(todoDTO);

        mockMvc.perform(put("https://localhost:8080/api/v1/todos/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(ObjectToJson.asJsonString(todoDTO))
                )
                .andDo(print())
                .andExpect(status().isNoContent());
    }

}
