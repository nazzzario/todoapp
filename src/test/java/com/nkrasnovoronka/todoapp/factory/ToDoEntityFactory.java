package com.nkrasnovoronka.todoapp.factory;

import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.model.TodoEntity;

import java.time.LocalDate;
import java.util.List;

public class ToDoEntityFactory {

    public static List<TodoEntity> getAllToDos() {
        TodoEntity todoEntity1 = new TodoEntity();
        todoEntity1.setId(1L);
        todoEntity1.setTaskHeader("Go to the shop");
        todoEntity1.setDescription("By milk");
        todoEntity1.setDeadline(LocalDate.parse("2021-04-12"));
        todoEntity1.setState(State.WORK_IN_PROGRESS);

        TodoEntity todoEntity2 = new TodoEntity();
        todoEntity2.setId(2L);
        todoEntity2.setTaskHeader("Finish project");
        todoEntity2.setDescription("Complete week4 task");
        todoEntity2.setDeadline(LocalDate.parse("2021-04-21"));
        todoEntity2.setState(State.PLANNED);

        return List.of(todoEntity1, todoEntity2);
    }

    public static List<TodoEntity> getAllToDosWithStatePlanned() {
        TodoEntity todoEntity = new TodoEntity();
        todoEntity.setId(1L);
        todoEntity.setTaskHeader("Finish project");
        todoEntity.setDescription("Complete week4 task");
        todoEntity.setDeadline(LocalDate.parse("2021-04-21"));
        todoEntity.setState(State.PLANNED);

        return List.of(todoEntity);
    }

    public static TodoEntity getToDoEntity(){
        TodoEntity todoEntity = new TodoEntity();
        todoEntity.setId(1L);
        todoEntity.setTaskHeader("Finish project");
        todoEntity.setDescription("Complete week4 task");
        todoEntity.setDeadline(LocalDate.parse("2021-04-21"));
        todoEntity.setState(State.PLANNED);
        return todoEntity;
    }



}
