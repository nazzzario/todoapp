package com.nkrasnovoronka.todoapp.service;

import com.nkrasnovoronka.todoapp.dto.CreateTodoDTO;
import com.nkrasnovoronka.todoapp.dto.TodoDTO;
import com.nkrasnovoronka.todoapp.exception.ToDoNotFoundException;
import com.nkrasnovoronka.todoapp.factory.ToDoEntityFactory;
import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.model.TodoEntity;
import com.nkrasnovoronka.todoapp.repository.TodoEntityRepository;
import com.nkrasnovoronka.todoapp.service.impl.TodoEntityServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TodoEntityServiceTest {

    @Mock
    private TodoEntityRepository todoEntityRepository;

    @InjectMocks
    private TodoEntityServiceImpl todoEntityService;

    @Test
    void getAllTodos() {
        //given
        when(todoEntityRepository.findAll()).thenReturn(ToDoEntityFactory.getAllToDos());

        //when
        List<TodoDTO> actual = todoEntityService.getAllTodos();

        //then
        TodoDTO todoDto1 = new TodoDTO();
        todoDto1.setTaskHeader("Go to the shop");
        todoDto1.setDescription("By milk");
        todoDto1.setDeadline(LocalDate.parse("2021-04-12"));
        todoDto1.setState(State.WORK_IN_PROGRESS);

        TodoDTO todoDto2 = new TodoDTO();
        todoDto2.setTaskHeader("Finish project");
        todoDto2.setDescription("Complete week4 task");
        todoDto2.setDeadline(LocalDate.parse("2021-04-21"));
        todoDto2.setState(State.PLANNED);
        List<TodoDTO> expected = List.of(todoDto1, todoDto2);

        assertEquals(expected, actual);
    }

    @Test
    void getAllTodosByState() {
        //given
        when(todoEntityRepository.findAllByState(State.PLANNED)).thenReturn(ToDoEntityFactory.getAllToDosWithStatePlanned());

        //when
        List<TodoDTO> actual = todoEntityService.getAllTodosByState(State.PLANNED);

        //then
        TodoDTO todoDTO = new TodoDTO();
        todoDTO.setTaskHeader("Finish project");
        todoDTO.setDescription("Complete week4 task");
        todoDTO.setDeadline(LocalDate.parse("2021-04-21"));
        todoDTO.setState(State.PLANNED);
        List<TodoDTO> expected = List.of(todoDTO);

        assertEquals(expected, actual);
    }

    @Test
    void createTodos() {
        when(todoEntityRepository.save(any(TodoEntity.class))).thenReturn(ToDoEntityFactory.getToDoEntity());

        CreateTodoDTO createTodoDTO = new CreateTodoDTO();
        createTodoDTO.setTaskHeader("Test todo header");
        createTodoDTO.setDescription("Test description");
        createTodoDTO.setDeadline(LocalDate.parse("2021-04-21"));


        todoEntityService.createTodos(createTodoDTO);

        verify(todoEntityRepository, times(1)).save(any(TodoEntity.class));
    }

    @Test
    void updateTodos() {
        when(todoEntityRepository.save(any(TodoEntity.class))).thenReturn(ToDoEntityFactory.getToDoEntity());
        when(todoEntityRepository.findById(anyLong())).thenReturn(Optional.of(ToDoEntityFactory.getToDoEntity()));

        TodoDTO todoDTO = new TodoDTO();
        todoDTO.setTaskHeader("Finish project");
        todoDTO.setDescription("Complete week4 task");
        todoDTO.setDeadline(LocalDate.parse("2021-04-21"));
        todoDTO.setState(State.PLANNED);

        todoEntityService.updateTodos(1L, todoDTO);

        verify(todoEntityRepository, times(1)).findById(anyLong());
        verify(todoEntityRepository, times(1)).save(any(TodoEntity.class));
    }

    @Test
    void getTodoById() {
        when(todoEntityRepository.findById(anyLong())).thenReturn(Optional.of(ToDoEntityFactory.getToDoEntity()));

        TodoDTO expected = new TodoDTO();
        expected.setTaskHeader("Finish project");
        expected.setDescription("Complete week4 task");
        expected.setDeadline(LocalDate.parse("2021-04-21"));
        expected.setState(State.PLANNED);

        TodoDTO actual = todoEntityService.getTodoById(1L);

        assertEquals(expected, actual);

    }

    @Test
    void deleteTodosById() {
        doNothing().when(todoEntityRepository).deleteById(anyLong());

        todoEntityService.deleteTodosById(1L);

        verify(todoEntityRepository, times(1)).deleteById(1L);
    }

    @Test
    void throwExceptionIfFindToDosWithInvalidState() {
        assertThrows(ToDoNotFoundException.class,
                () -> todoEntityService.getAllTodosByState(null));
    }

    @Test
    void throwExceptionIfCannotCreateToDo() {
        assertThrows(IllegalArgumentException.class,
                () -> todoEntityService.createTodos(null));
    }

    @Test
    void throwExceptionIfCannotFindToDoWhenUpdate() {
        assertThrows(ToDoNotFoundException.class,
                () -> todoEntityService.updateTodos(null, null));
    }



}
