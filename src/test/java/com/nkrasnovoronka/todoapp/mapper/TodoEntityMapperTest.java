package com.nkrasnovoronka.todoapp.mapper;

import com.nkrasnovoronka.todoapp.dto.CreateTodoDTO;
import com.nkrasnovoronka.todoapp.dto.TodoDTO;
import com.nkrasnovoronka.todoapp.exception.InvalidStateException;
import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.model.TodoEntity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TodoEntityMapperTest {

    static TodoEntity todoEntity;

    static TodoDTO todoDTO;

    @BeforeAll
    static void setUp() {
        todoEntity = new TodoEntity();
        todoEntity.setId(1L);
        todoEntity.setTaskHeader("Mapper entity test");
        todoEntity.setDescription("Mapper entity test");
        todoEntity.setDeadline(LocalDate.parse("2021-10-15"));
        todoEntity.setState(State.PLANNED);

        todoDTO = new TodoDTO();
        todoDTO.setTaskHeader("Mapper dto test");
        todoDTO.setDescription("Mapper dto test");
        todoDTO.setDeadline(LocalDate.parse("2021-10-14"));
        todoDTO.setState(State.PLANNED);
    }

    @Test
    void dtoToEntityMapperTest(){
        TodoEntity expected = new TodoEntity();
        expected.setTaskHeader("Mapper dto test");
        expected.setDescription("Mapper dto test");
        expected.setDeadline(LocalDate.parse("2021-10-14"));
        expected.setState(State.PLANNED);

        TodoEntity actual = TodoEntityMapper.toEntity(todoDTO);

        assertEquals(expected, actual);
    }

    @Test
    void createdDtoToEntityMapperTest(){
        TodoEntity expected = new TodoEntity();
        expected.setTaskHeader("Mapper dto test");
        expected.setDescription("Mapper dto test");
        expected.setDeadline(LocalDate.parse("2021-10-14"));

        CreateTodoDTO createTodoDTO = new CreateTodoDTO();
        createTodoDTO.setTaskHeader("Mapper dto test");
        createTodoDTO.setDescription("Mapper dto test");
        createTodoDTO.setDeadline(LocalDate.parse("2021-10-14"));

        TodoEntity actual = TodoEntityMapper.toEntity(createTodoDTO);
        assertEquals(expected, actual);
    }

    @Test
    void entityToDtoMapper(){
        TodoDTO expected = new TodoDTO();
        expected.setTaskHeader("Mapper entity test");
        expected.setDescription("Mapper entity test");
        expected.setDeadline(LocalDate.parse("2021-10-15"));
        expected.setState(State.PLANNED);

        TodoDTO actual = TodoEntityMapper.toDto(todoEntity);

        assertEquals(expected, actual);
    }

    @Test
    void updateEntityTest(){
        TodoDTO updated = new TodoDTO();
        updated.setTaskHeader("Updated");
        updated.setDescription("Updated");
        updated.setDeadline(LocalDate.parse("2021-10-15"));
        updated.setState(State.WORK_IN_PROGRESS);

        TodoEntity expected = new TodoEntity();
        expected.setId(1L);
        expected.setTaskHeader("Updated");
        expected.setDescription("Updated");
        expected.setDeadline(LocalDate.parse("2021-10-15"));
        expected.setState(State.WORK_IN_PROGRESS);


        TodoEntity actual = TodoEntityMapper.updateEntity(TodoEntityMapperTest.todoEntity, updated);

        assertEquals(expected, actual);
    }

    @Test
    void throwAnExceptionIfNotValidUpdatedState(){
        TodoDTO updated = new TodoDTO();
        updated.setTaskHeader("Updated");
        updated.setDescription("Updated");
        updated.setDeadline(LocalDate.parse("2021-10-15"));
        updated.setState(State.DONE);

        assertThrows(InvalidStateException.class,
                () -> TodoEntityMapper.updateEntity(todoEntity, updated));

    }

}
