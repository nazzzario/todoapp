package com.nkrasnovoronka.todoapp.repository;

import com.nkrasnovoronka.todoapp.factory.ToDoEntityFactory;
import com.nkrasnovoronka.todoapp.model.State;
import com.nkrasnovoronka.todoapp.model.TodoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop"
})
class ToDoEntityRepositoryTest {

    @Autowired
    private TodoEntityRepository todoEntityRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void findAllByState() {
        List<TodoEntity> todoEntities = prepareDatabase();
        System.out.println(todoEntities);

        List<TodoEntity> allByState = todoEntityRepository.findAllByState(State.WORK_IN_PROGRESS);
        assertEquals(todoEntities.get(1), allByState.get(0));

    }

    private List<TodoEntity> prepareDatabase() {
        List<TodoEntity> allToDos = ToDoEntityFactory.getAllToDos();
        allToDos.stream()
                .peek(todoEntity -> todoEntity.setId(null))
                .forEach(testEntityManager::persistAndFlush);

        TodoEntity todoEntity = testEntityManager.find(TodoEntity.class, 2L);
        todoEntity.setState(State.WORK_IN_PROGRESS);
        return allToDos;
    }
}
